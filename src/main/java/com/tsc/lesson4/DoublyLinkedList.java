package com.tsc.lesson4;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class DoublyLinkedList<V> extends AbstractList<V>
        implements List<V> {

    private int size = 0;
    private Element<V> first;
    private Element<V> last;
    private int modCount = 0;

    /**
     * Возвращает количество элементов в списке
     *
     * @return количество элементов в списке
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Проверяет, содержится ли объект в списке
     *
     * @param o объект для проверки
     * @return true, если объект содержится в массиве, иначе false
     */
    @Override
    public boolean contains(final Object o) {
        final Element<V> result = getElementByValue(o);
        if (nonNull(result)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает массив, содержащий все элементы списка.
     *
     * @return массив, содержащий все элементы списка.
     */
    @Override
    public Object[] toArray() {
        final Object[] resultArray = new Object[size];
        int i = 0;
        for (Element<V> element = first; element != null; element = element.next) {
            resultArray[i] = element.value;
            i++;
        }
        return resultArray;
    }

    /**
     * Устанавливает новое значение элемента в указаном индексе
     *
     * @param index индекс, по которому находится элемент
     * @param value новое значение
     * @return прежнее значение эелемента
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public V set(final int index, final V value) {
        checkIndex(index);
        final Element<V> element = getElementByIndex(index);
        V oldValue = element.value;
        element.value = value;
        return oldValue;
    }

    /**
     * Возвращает индекс первого вхождения указанного элемента в этом списке
     * или -1, если этот список не содержит элемент.
     *
     * @param o элемент для поиска
     * @return индекс первого вхождения эелемента в список. Если элемент не найден, то возвращает -1.
     */
    @Override
    public int indexOf(final Object o) {
        int i = 0;
        if (isNull(o)) {
            for (Element<V> element = first; element != null; element = element.next) {
                if (isNull(element.value)) {
                    return i;
                }
                i++;
            }
        } else {
            for (Element<V> element = first; element != null; element = element.next) {
                if (element.value.equals(o)) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    /**
     * Возвращает объект ListIterator для текущего списка начиная с первого элемента
     *
     * @return итератор элементов для текущего списка
     */
    @Override
    public ListIterator<V> listIterator() {
        return new ListIteratorImpl(0);
    }

    /**
     * Добавляет объект в конец списка
     *
     * @param val объект, который будет добавлен
     * @return true, если объект добавлен
     */
    public boolean add(final V val) {
        addElementToEnd(val);
        return true;
    }

    /**
     * Добавляет объект в список, по указанному индексу. Элемент, находящийся по этому индексу, и все последеющие
     * сдвигается вправо на одну позицию.
     *
     * @param index индекс по которому добавляется новый элемент
     * @param val   объект для добавления
     */
    public void add(final int index, final V val) {
        checkIndex(index);
        if (index == size) {
            addElementToEnd(val);
        } else {
            addElementToMiddle(val, getElementByIndex(index));
        }
    }

    /**
     * Удаляет первое вхождение объекта из списка, если список содержит этот объект.
     *
     * @param val объект для удаления
     * @return true, если объект был удален, иначе false
     */
    public boolean remove(final Object val) {
        final Element<V> element = getElementByValue(val);
        if (nonNull(element)) {
            unlink(element);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Удаляет из списка элемент, находящийся по указанному индексу.
     *
     * @param index индекс элемента в списке
     * @return удаленный элемент
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public V remove(final int index) {
        checkIndex(index);
        final Element<V> removedElement = getElementByIndex(index);
        final V removedValue = unlink(removedElement);
        return removedValue;
    }

    /**
     * Возвращает элемент списка по указаному индексу
     *
     * @param index индекс элемента в списке
     * @return элемент в указанной позиции
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public V get(final int index) {
        checkIndex(index);
        final V result = getElementByIndex(index).value;
        return result;
    }

    private void checkIndex(final int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    private Element<V> getElementByIndex(final int index) {
        Element<V> result;
        if (index < size / 2) {
            result = first;
            for (int i = 0; i < index; i++) {
                result = result.next;
            }
        } else {
            result = last;
            for (int i = size - 1; i > index; i--) {
                result = result.prev;
            }
        }
        return result;
    }

    private Element<V> getElementByValue(final Object value) {
        Element<V> result = null;
        if (isNull(value)) {
            for (Element<V> i = first; i != null; i = i.next) {
                if (isNull(i.value)) {
                    result = i;
                    break;
                }
            }
        } else {
            for (Element<V> i = first; i != null; i = i.next) {
                if (i.value.equals(value)) {
                    result = i;
                    break;
                }
            }
        }
        return result;
    }

    private void addElementToEnd(final V value) {
        final Element<V> oldLastElement = last;
        final Element<V> newElement = new Element(last, null, value);
        last = newElement;
        if (isNull(oldLastElement)) {
            first = newElement;
        } else {
            oldLastElement.next = newElement;
        }
        size++;
        modCount++;
    }

    private void addElementToMiddle(final V value, final Element<V> element) {
        final Element prev = element.prev;
        final Element newElement = new Element(prev, element, value);
        element.prev = newElement;
        if (isNull(prev)) {
            first = newElement;
        } else {
            prev.next = newElement;
        }
        size++;
        modCount++;
    }

    private V unlink(final Element<V> element) {
        final Element<V> prevElement = element.prev;
        final Element<V> nextElement = element.next;
        final V removedElementValue = element.value;
        element.next = element.prev = null;
        element.value = null;

        if (isNull(prevElement)) {
            first = nextElement;
        } else {
            prevElement.next = nextElement;
        }

        if (isNull(nextElement)) {
            last = prevElement;
        } else {
            nextElement.prev = prevElement;
        }
        size--;
        modCount++;
        return removedElementValue;
    }

    private class Element<V> {
        Element<V> prev;
        Element<V> next;
        V value;

        Element(final Element<V> prev,
                final Element<V> next,
                final V value) {
            this.prev = prev;
            this.next = next;
            this.value = value;
        }
    }

    private class ListIteratorImpl implements ListIterator<V> {

        private Element<V> next;
        private Element<V> lastReturn;
        private int expectedModCount = modCount;
        private int nextIndex;

        public ListIteratorImpl(final int idx) {
            checkIndex(idx);
            if (idx == size) {
                next = null;
            } else {
                next = getElementByIndex(idx);
            }
            this.nextIndex = idx;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public V next() {
            checkForCountModification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastReturn = next;
            next = next.next;
            nextIndex++;
            return lastReturn.value;
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        @Override
        public V previous() {
            checkForCountModification();
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            if (isNull(next)) {
                lastReturn = next = last;
            } else {
                lastReturn = next = next.prev;
            }
            nextIndex--;
            return lastReturn.value;

        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            checkForCountModification();
            if (isNull(lastReturn)) {
                throw new IllegalStateException();
            }
            final Element<V> lastNext = lastReturn.next;
            DoublyLinkedList.this.unlink(lastReturn);
            if (next == lastReturn)
                next = lastNext;
            else
                nextIndex--;
            lastReturn = null;
            expectedModCount = modCount;
        }

        @Override
        public void set(final V v) {
            if (isNull(lastReturn))
                throw new IllegalStateException();
            checkForCountModification();
            lastReturn.value = v;
        }

        @Override
        public void add(final V v) {
            checkForCountModification();
            lastReturn = null;
            DoublyLinkedList.this.add(nextIndex, v);
            expectedModCount = modCount;
            nextIndex++;
        }

        void checkForCountModification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }
}
