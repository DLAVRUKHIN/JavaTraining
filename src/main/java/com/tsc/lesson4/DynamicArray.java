package com.tsc.lesson4;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DynamicArray<T> extends AbstractList<T>
                                implements List<T> {

    private Object[] storageArray;
    private final int DEFAULT_STORAGE_SIZE = 10;
    private int elementsAmount = 0;
    private int modCount = 0;

    public DynamicArray() {
        storageArray = new Object[DEFAULT_STORAGE_SIZE];
    }

    public DynamicArray(T[] storageArray) {
        elementsAmount = storageArray.length;
        this.storageArray = storageArray;

    }

    public DynamicArray(final int initialStorageCapacity) {
        if (initialStorageCapacity >= 0) {
            storageArray = new Object[initialStorageCapacity];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialStorageCapacity);
        }
    }

    /**
     * Добавляет объект в конец массива
     *
     * @param e объект для добавления
     * @return true, если объект добавлен
     */
    public boolean add(final T e) {
        modCount++;
        final int length = storageArray.length;
        if (elementsAmount == length) {
            expand();
        }
        storageArray[elementsAmount] = e;
        elementsAmount++;
        return true;
    }

    /**
     * Добавляет элемент в указанную позицию.
     *
     * @param i индекс в массиве
     * @param e элемент для вставки
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public void add(final int i, final T e) {
        checkIndex(i);
        modCount++;
        final int length = storageArray.length;
        if (elementsAmount == length) {
            expand();
        }
        System.arraycopy(storageArray, i, storageArray, i + 1, elementsAmount - i);
        storageArray[i] = e;
        elementsAmount++;
    }

    /**
     * Установить значение newValue у элемента с индексом i
     *
     * @param i        индекс эелемента в массиве
     * @param newValue новое значение элемента
     * @return старое значение элемента
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public T set(final int i, final T newValue) {
        checkIndex(i);
        final T oldValue = (T) storageArray[i];
        storageArray[i] = newValue;
        return oldValue;
    }

    /**
     * Возвращает элемент с индексом i
     *
     * @param i индекс элемента массива
     * @return элемент в указанной позиции
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public T get(final int i) {
        checkIndex(i);
        return (T) storageArray[i];
    }

    /**
     * удаление элемента из массива в указанной позиции
     *
     * @param i индекс удаляемого элемента
     * @return удаленный элемент в указанной позиции
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public T remove(final int i) {
        checkIndex(i);
        final T oldValue = (T) storageArray[i];
        removeElement(i);
        return oldValue;
    }

    /**
     * Удаляет объект из массива.
     * Если таких объектов несколько, то удаляется первый найденный
     *
     * @param e объект, который требуется удалить
     * @return true, если объект удален
     */
    public boolean remove(final Object e) {
        final int idx = findIndexObjectInRepository(e);
        if (idx >= 0) {
            removeElement(idx);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает колическто элементов в массиве
     *
     * @return количество элементов
     */
    public int size() {
        return elementsAmount;
    }


    /**
     * Возвращает позицию элемента в массиве.
     * Если таких объектов несколько, возвращается индекс первого найденного элемента.
     * Если объект не найден, возвращает -1
     *
     * @param e объект для поиска
     * @return индекс объекта в массиве
     */
    public int indexOf(final Object e) {
        final int idx = findIndexObjectInRepository(e);
        return idx;
    }

    /**
     * Проверка, содержит ли массив переданный объект
     *
     * @param e объект для проверки
     * @return true, если объект содержится в массиве, иначе false
     */
    public boolean contains(final Object e) {
        final int idx = findIndexObjectInRepository(e);
        return idx >= 0;
    }

    /**
     * Возвращает массив объектов
     *
     * @return массив объектов
     */
    public Object[] toArray() {
        return Arrays.copyOf(storageArray, elementsAmount);
    }

    public ListIterator listIterator() {
        return new DynamicArray.ListIteratorImpl(0);
    }

    /**
     * Увеличивает размер массива в 1.5 раза
     */
    private void expand() {
        final int oldSize = storageArray.length;
        final int newSize = (int) Math.ceil(oldSize * 1.5);
        storageArray = Arrays.copyOf(storageArray, newSize);
    }

    private void checkIndex(final int i) {
        if (i > elementsAmount || i < 0) {
            throw new ArrayIndexOutOfBoundsException(i);
        }
    }

    private void removeElement(final int i) {
        modCount++;
        elementsAmount--;
        if (elementsAmount > i) {
            System.arraycopy(storageArray, i + 1, storageArray, i, elementsAmount - i);
        }
        storageArray[elementsAmount] = null;
    }

    private int findIndexObjectInRepository(final Object e) {
        int idx = -1;
        if (e == null) {
            for (int i = 0; i < elementsAmount; i++) {
                if (storageArray[i] == null) {
                    idx = i;
                    break;
                }
            }
        } else {
            for (int i = 0; i < elementsAmount; i++) {
                if (storageArray[i].equals(e)) {
                    idx = i;
                    break;
                }
            }
        }
        return idx;
    }

    private class ListIteratorImpl implements ListIterator<T> {

        private int currentIndex;
        private int lastReturn = -1;
        private int expectedModCount = modCount;

        public ListIteratorImpl(final int currentIndex) {
            this.currentIndex = currentIndex;
        }

        public boolean hasNext() {
            return currentIndex < elementsAmount;
        }

        public boolean hasPrevious() {
            return currentIndex != 0;
        }

        public T next() {
            checkModCount();
            if (currentIndex > elementsAmount - 1) {
                throw new NoSuchElementException();
            }
            final Object result = storageArray[currentIndex];
            lastReturn = currentIndex;
            currentIndex++;
            return (T) result;
        }

        public T previous() {
            checkModCount();
            final int i = currentIndex - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            currentIndex = i;
            return (T) storageArray[i];
        }

        public int nextIndex() {
            return currentIndex;
        }

        public int previousIndex() {
            return currentIndex - 1;
        }

        public void remove() {
            if (lastReturn < 0) {
                throw new IllegalStateException();
            }
            checkModCount();
            DynamicArray.this.remove(lastReturn);
            currentIndex = lastReturn;
            lastReturn = -1;
            expectedModCount = modCount;
        }

        public void set(T e) {
            checkModCount();
            if (lastReturn < 0) {
                throw new IllegalStateException();
            }
            DynamicArray.this.set(lastReturn, e);
        }

        public void add(T e) {
            checkModCount();
            DynamicArray.this.add(currentIndex, e);
            currentIndex++;
            lastReturn = -1;
            expectedModCount = modCount;
        }

        private void checkModCount() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
