package com.tsc.lesson3;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * Реализация динамически расширяемого массива.
 * Данные хранятся в той последовательности, в которой были добавлены.
 * При нехватке места, размер массива увеличится в 1.5 раза.
 *
 * @author Дмитрий Лаврухин
 */
public class DynamicArray {

    private static final int DEFAULT_STORAGE_SIZE = 10;

    private Object[] storageArray;
    private int elementsAmount = 0;
    private int modCount = 0;

    /**
     * Конструктор, создающий пустой массив размерностью 10.
     */
    public DynamicArray() {
        storageArray = new Object[DEFAULT_STORAGE_SIZE];
    }

    /**
     * Конструктов, создающий массив из элементов переданного массива.
     *
     * @param storageArray массив данных.
     */
    public DynamicArray(Object[] storageArray) {
        elementsAmount = storageArray.length;
        this.storageArray = storageArray;

    }

    /**
     * Конструктор, создающий пустой массив указаной размерности.
     *
     * @param initialStorageCapacity начальная размерность массива.
     */
    public DynamicArray(final int initialStorageCapacity) {
        if (initialStorageCapacity >= 0) {
            storageArray = new Object[initialStorageCapacity];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialStorageCapacity);
        }
    }

    /**
     * Добавляет объект в конец массива
     *
     * @param e объект для добавления
     * @return true, если объект добавлен
     */
    public boolean add(final Object e) {
        final int length = storageArray.length;
        if (elementsAmount == length) {
            expand();
        }
        storageArray[elementsAmount] = e;
        elementsAmount++;
        modCount++;
        return true;
    }

    /**
     * Добавляет элемент в указанную позицию.
     *
     * @param i индекс в массиве
     * @param e элемент для вставки
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public void add(final int i, final Object e) {
        checkIndex(i);
        final int length = storageArray.length;
        if (elementsAmount == length) {
            expand();
        }
        System.arraycopy(storageArray, i, storageArray, i + 1, elementsAmount - i);
        storageArray[i] = e;
        elementsAmount++;
        modCount++;
    }

    /**
     * Установить значение newValue у элемента с индексом i
     *
     * @param i        индекс эелемента в массиве
     * @param newValue новое значение элемента
     * @return старое значение элемента
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public Object set(final int i, final Object newValue) {
        checkIndex(i);
        final Object oldValue = storageArray[i];
        storageArray[i] = newValue;
        return oldValue;
    }

    /**
     * Возвращает элемент с индексом i
     *
     * @param i индекс элемента массива
     * @return элемент в указанной позиции
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public Object get(final int i) {
        checkIndex(i);
        return storageArray[i];
    }

    /**
     * удаление элемента из массива в указанной позиции
     *
     * @param i индекс удаляемого элемента
     * @return удаленный элемент в указанной позиции
     * @throws ArrayIndexOutOfBoundsException {@inheritDoc}
     */
    public Object remove(final int i) {
        checkIndex(i);
        final Object oldValue = storageArray[i];
        removeElement(i);
        return oldValue;
    }

    /**
     * Удаляет объект из массива.
     * Если таких объектов несколько, то удаляется первый найденный
     *
     * @param e объект, который требуется удалить
     * @return true, если объект удален
     */
    public boolean remove(final Object e) {
        final int idx = findIndexObjectInRepository(e);
        if (idx >= 0) {
            removeElement(idx);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает колическто элементов в массиве
     *
     * @return количество элементов
     */
    public int size() {
        return elementsAmount;
    }

    /**
     * Возвращает позицию элемента в массиве.
     * Если таких объектов несколько, возвращается индекс первого найденного элемента.
     * Если объект не найден, возвращает -1
     *
     * @param e объект для поиска
     * @return индекс объекта в массиве
     */
    public int indexOf(final Object e) {
        final int idx = findIndexObjectInRepository(e);
        return idx;
    }

    /**
     * Проверка, содержит ли массив переданный объект
     *
     * @param e объект для проверки
     * @return true, если объект содержится в массиве, иначе false
     */
    public boolean contains(final Object e) {
        final int idx = findIndexObjectInRepository(e);
        return idx >= 0;
    }

    /**
     * Возвращает массив объектов
     *
     * @return массив объектов
     */
    public Object[] toArray() {
        return Arrays.copyOf(storageArray, elementsAmount);
    }

    /**
     * Возвращает итетарор, начиная с 0 элемента массива.
     *
     * @return объект DynamicArray.ListIterator
     */
    public DynamicArray.ListIterator listIterator() {
        return new ListIterator(0);
    }

    /**
     * Увеличивает размер массива в 1.5 раза
     */
    private void expand() {
        final int oldSize = storageArray.length;
        final int newSize = (int) Math.ceil(oldSize * 1.5);
        storageArray = Arrays.copyOf(storageArray, newSize);
    }

    private void checkIndex(final int i) {
        if (i > elementsAmount || i < 0) {
            throw new ArrayIndexOutOfBoundsException(i);
        }
    }

    private void removeElement(final int i) {
        modCount++;
        elementsAmount--;
        if (elementsAmount > i) {
            System.arraycopy(storageArray, i + 1, storageArray, i, elementsAmount - i);
        }
        storageArray[elementsAmount] = null;
    }

    private int findIndexObjectInRepository(final Object e) {
        int idx = -1;
        if (e == null) {
            for (int i = 0; i < elementsAmount; i++) {
                if (storageArray[i] == null) {
                    idx = i;
                    break;
                }
            }
        } else {
            for (int i = 0; i < elementsAmount; i++) {
                if (storageArray[i].equals(e)) {
                    idx = i;
                    break;
                }
            }
        }
        return idx;
    }

    public class ListIterator {

        private int currentIndex;
        private int lastReturn = -1;
        private int expectedModCount = modCount;

        /**
         * Конструктор, устанавливающий индекс элемента,
         * с которого начнется итерирование.
         *
         * @param currentIndex индекс элемента в массиве.
         */
        public ListIterator(final int currentIndex) {
            this.currentIndex = currentIndex;
        }

        /**
         * Проверяет, если в массиве следующий элемент.
         *
         * @return true, если в массиве есть следующий элемент.
         */
        public boolean hasNext() {
            return currentIndex < elementsAmount;
        }

        /**
         * Проверяет, если в массиве предыдущий элемент.
         *
         * @return true, если в массиве есть предыдущий элемент.
         */
        public boolean hasPrevious() {
            return currentIndex != 0;
        }

        /**
         * Возвращает следуюший элемент массива.
         *
         * @return следующий элемент массива.
         * @throws ConcurrentModificationException если массив был изменен методами не итератора.
         * @throws NoSuchElementException          если в массиве отсутствует следующий элемент.
         */
        public Object next() {
            checkModCount();
            if (currentIndex > elementsAmount - 1) {
                throw new NoSuchElementException();
            }
            final Object result = storageArray[currentIndex];
            lastReturn = currentIndex;
            currentIndex++;
            return result;
        }

        /**
         * Возвращает предыдущий элемент массива.
         *
         * @return предыдущий элемент массива.
         * @throws ConcurrentModificationException если массив был изменен не методами итератора.
         * @throws NoSuchElementException          если в массиве отсутствует предыдущий элемент.
         */
        public Object previous() {
            checkModCount();
            final int i = currentIndex - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            currentIndex = i;
            lastReturn = i;
            return storageArray[i];
        }

        /**
         * Возвращает индекс элемента, который будет возвращен методом next.
         *
         * @return индекс элемента в массиве.
         */
        public int nextIndex() {
            return currentIndex;
        }

        /**
         * Возвращает индекс элемента, который будет возвращен методом previous.
         *
         * @return индекс элемента в массиве.
         */
        public int previousIndex() {
            return currentIndex - 1;
        }

        /**
         * Удаляет последний возвращенный элемент из массива.
         */
        public void remove() {
            if (lastReturn < 0) {
                throw new IllegalStateException();
            }
            checkModCount();
            DynamicArray.this.remove(lastReturn);
            currentIndex = lastReturn;
            lastReturn = -1;
            expectedModCount = modCount;
        }

        /**
         * Устанавливает новое значение у последнего полученного элемента.
         *
         * @param e новое значение элемента.
         */
        public void set(Object e) {
            checkModCount();
            if (lastReturn < 0) {
                throw new IllegalStateException();
            }
            DynamicArray.this.set(lastReturn, e);
        }

        /**
         * Добавляет новый элемент в массив, перед последним возвращенныи элементом.
         *
         * @param e значение для добавления.
         * @throws IllegalStateException           если в результате предыдущей опрерации над массивом его размер был изменен.
         * @throws ConcurrentModificationException если в
         */
        public void add(Object e) {
            checkModCount();
            DynamicArray.this.add(currentIndex, e);
            currentIndex++;
            lastReturn = -1;
            expectedModCount = modCount;
        }

        private void checkModCount() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
