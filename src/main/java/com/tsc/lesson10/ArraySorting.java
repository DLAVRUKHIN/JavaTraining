package com.tsc.lesson10;

import static java.util.Objects.nonNull;

/**
 * Класс, реализующий методы сортировки массива.
 *
 * @author Лаврухин Дмитрий
 */
public class ArraySorting {

    /**
     * Метод, реализующий сортировку массива, алгоритмом QuickSort.
     *
     * @param array массив для сортировки
     */
    public static void quickSort(int[] array) {
        if (nonNull(array) && array.length > 1) {
            sort(array, 0, array.length - 1);
        }
    }

    private static void sort(int[] array, int low, int high) {
        if (low >= high) {
            return;
        }

        int middle = low + (high - low) / 2;
        int middleElement = array[middle];

        int leftIndex = low;
        int rightIndex = high;
        while (leftIndex <= rightIndex) {
            while (array[leftIndex] < middleElement) {
                leftIndex++;
            }
            while (array[rightIndex] > middleElement) {
                rightIndex--;
            }
            if (leftIndex <= rightIndex) {
                swapElement(array, leftIndex, rightIndex);
                leftIndex++;
                rightIndex--;
            }
        }

        if (low < rightIndex) {
            sort(array, low, rightIndex);
        }
        if (high > leftIndex) {
            sort(array, leftIndex, high);
        }
    }

    private static void swapElement(int[] array, int firstIndex, int secondIndex) {
        int tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
    }
}
