package com.tsc.lesson1;

public class Solution {

    /**
     * Решение задачи 6.a.
     *
     * @param n степень, для возведения в неё 2
     * @return 2^n
     */
    public static int getPowerTwo(final int n){
        final int result = 1 << n;
        return result;
    }

    /**
     *Решение задачи 6.b.
     * @param n степень для первого слагаемого
     * @param m степень для второго слагаемого
     * @return 2^n + 2^m
     */
    public static long getSumPowerTwo(final int n, final int m){
        final long result;
        if(m == n){
            result = 1L << n+1;
        }else{
            final int a = 1 << n;
            final int b = 1 << m;
            result = a ^ b;
        }
        return result;
    }

    /**
     * Решение задачи 6.c. Устанавливает lowerBitCount бит числа a в 0
     * @param a исходное число
     * @param lowerBitCount количество младших бит
     * @return исходное число, у которого lowerBitCount бит равно 0
     */
    public static int resetLowerBit(final int a, final int lowerBitCount){
        final int mask = -1<<lowerBitCount;
        final int result = a & mask;
        return result;
    }

    /**
     * Решение задачи 6.d. Устанавливает бит числа a, порядковый номер которого равен bitNumber, равным 1.
     * @param a исходное число
     * @param bitNumber порядковый номер бита в числе a
     * @return измененное исходное число
     */
    public static int setBitToOne(final int a, final int bitNumber){
        final int mask = 1 << bitNumber;
        final int result = a | mask;
        return result;
    }

    /**
     * Решение задачи 6.e. Инвертирует бит, с порядковым номерои bitNumber, в числе a.
     * @param a исходное числа
     * @param bitNumber порядковый номер бита
     * @return измененное число a
     */
    public static int invertBit(final int a, final int bitNumber){
        final int mask = 1 << bitNumber;
        final int result = a ^ mask;
        return result;
    }

    /**
     * Решение задачи 6.d. Устанавливает бит числа a, порядковый номер которого равен bitNumber, равным 0.
     * @param a исходное число
     * @param bitNumber порядковый номер бита в числе a
     * @return измененное исходное число
     */
    public static int setBitToZero(final int a, final int bitNumber){
        final int mask = ~(1 << bitNumber);
        final int result = a & mask;
        return result;
    }

    /**
     * Решение задачи 6.g.
     * @param a исходное число
     * @param lowerBitCount количество бит, которые необходимо получить
     * @return lowerBitCount младших бит числа a.
     */
    public static int getLowerBits(final int a, final int lowerBitCount){
        final int mask = ~(-1 << lowerBitCount);
        final int result = a & mask;
        return result;
    }

    /**
     * Решение задачи 6.h.
     * @param a исходное число
     * @param bitNumber порядковый номер бита
     * @return бит, с порядковым номером bitNumber, числа a.
     */
    public static int getBit(final int a, final int bitNumber){
        final int result = (a >>> bitNumber) & 1;
        return  result;
    }

    public static String binaryRepresentationOfNumber(final byte a){
        StringBuilder binaryRepresent = new StringBuilder();

        for (int i = 0; i <= 7; i++){
            binaryRepresent.append(a >> i & 1);
        }
        return binaryRepresent.reverse().toString();
    }

    /**
     * Решение задачи 7.
     * Обмен значениями двух переменных, с помощью сложения
     * @param arr массив содержащий два числа
     */
    public static void swapTwoVariablesAdd(final int[] arr){
        arr[0] += arr[1];
        arr[1]  = arr[0] - arr[1];
        arr[0] -= arr[1];
    }

    /**
     *Решение задачи 7.
     *Обмен значениями двух переменных, с помощью умножения
     *@param arr массив содержащий два числа
     */
    public static void swapTwoVariablesMultiplication(final int[] arr){
        arr[0] *= arr[1];
        arr[1] = arr[0] / arr[1];
        arr[0] /= arr[1];
    }

    /**
     *Решение задачи 7.
     *Обмен значениями двух переменных, с помощью исключающего или
     *@param arr массив содержащий два числа
     */
    public static void swapTwoVariablesXOR(final int[] arr){
        arr[0] ^= arr[1];
        arr[1] = arr[0] ^ arr[1];
        arr[0] ^= arr[1];
    }

    /**
     * Решение задачи 7.
     * Обмен значениями двух переменных, с помощью смешанного способа
     * @param arr массив содержащий два числа
     */
    public static void swapTwoVarsMix(final int[] arr){
        arr[0] = (arr[0] & arr[1]) + (arr[0] | arr[1]);
        arr[1] = arr[0] + ~arr[1] + 1;
        arr[0] = arr[0] + ~arr[1] + 1;
    }
}
