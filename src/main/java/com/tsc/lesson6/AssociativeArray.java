package com.tsc.lesson6;

import static java.lang.Float.isNaN;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Реализация ассоциативного массива
 *
 * @param <K> тип ключа элемента
 * @param <V> тип значения элемента
 * @author Дмитрий Лаврухин
 */
public class AssociativeArray<K, V> {

    private static final int DEFAULT_STORAGE_SIZE = 16;
    private static final int MAXIMUM_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private final float loadFactor;
    private int size;
    private int threshold;
    private Node<K, V>[] storage;

    /**
     * Создание коллекции вместимостью по умолчанию
     */
    public AssociativeArray() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
    }

    /**
     * Создание коллекции с заданной начальной вместимостью и коэффициентом загрузки
     *
     * @param initialSize - начальная вместимость
     * @param loadFactor  - коэффициент загрузки
     * @throws IllegalArgumentException - если передана отрицательная вместимость или некорректный коэффициент загрузки
     */
    public AssociativeArray(int initialSize, float loadFactor) {
        if (initialSize < 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + initialSize);
        }
        if (initialSize > MAXIMUM_CAPACITY) {
            initialSize = MAXIMUM_CAPACITY;
        }
        if (loadFactor <= 0 || isNaN(loadFactor)) {
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
        }
        this.loadFactor = loadFactor;
        this.threshold = tableSizeFor(initialSize);
    }

    /**
     * Создание коллекции с заданной начальной вместимостью
     *
     * @param initialSize - начальная вместимость
     */
    public AssociativeArray(int initialSize) {
        this(initialSize, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Добавляет в коллекцию новый элемент, с переданными ключем и значением.
     * Если элемент с таким ключем уже существует, то заменяет его значение на переданное.
     *
     * @param key ключ элемента
     * @param val значение элемента
     * @return старое значение элемента, если он существует в коллекции. Иначе null.
     */
    public V add(K key, V val) {
        if (storage == null || storage.length == 0) {
            resize();
        }

        int hash = getHash(key);
        int index = getIndexInStorage(hash);
        Node<K, V> newNode = new Node<>(hash, key, val);
        return addValue(newNode, index);
    }

    /**
     * Возвращает значение элемента, у которого ключ равен переданному.
     *
     * @param key ключ элемента
     * @return значение, если элемент существует. Иначе null.
     */
    public V get(K key) {
        if (nonNull(storage) && storage.length > 0) {
            int hash = getHash(key);
            int index = getIndexInStorage(hash);
            Node<K, V> firstNode = storage[index];
            if (nonNull(firstNode)) {
                Node<K, V> findNode = findElementByKey(key, firstNode);
                if (nonNull(findNode)) {
                    return findNode.value;
                }
            }
        }
        return null;
    }

    /**
     * Удаляет из коллекции элемент, у которого ключ равен переданному
     *
     * @param key - ключ элемента
     * @return значение элемента, если элемент был удален. Иначе null.
     */
    public V remove(K key) {
        if (nonNull(storage) && storage.length > 0) {
            int hash = getHash(key);
            int index = getIndexInStorage(hash);
            Node<K, V> firstNode = storage[index];
            if (nonNull(firstNode)) {
                if (firstNode.equalsKey(key)) {
                    storage[index] = firstNode.next;
                    size--;
                    return firstNode.value;
                }
                return removeElementByKey(key, firstNode);
            }
        }
        return null;
    }

    /**
     * Строковое представление коллецкции
     *
     * @return строка, содержащая все пары ключ/значение, которые хранятся в коллекции
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("{ ");
        for (Node<K, V> node : storage) {
            while (nonNull(node)) {
                result.append(node);
                node = node.next;
            }
        }
        result.append("}");
        return result.toString();
    }

    /**
     * Добавляет новый элемент в таблицу.
     * В случае возникновения коллизии, элемент помещается в конец односвязного списка.
     * Если элемент с ключем уже существует, то его значение заменяется новым,
     * метод возвращает его старое значение.
     *
     * @param newNode элемент для добавления
     * @param index индекс для добавления элемента
     * @return старое значение элемента, если он уже существует в таблице.
     */
    private V addValue(Node<K, V> newNode, int index) {
        Node<K, V> node = storage[index];
        K newKey = newNode.key;
        if (isNull(node)) {
            storage[index] = newNode;
        } else {
            Node<K, V> previousNode = node;
            for (; nonNull(node); node = node.next) {
                if (node.equalsKey(newKey)) {
                    V oldValue = node.value;
                    node.value = newNode.value;
                    return oldValue;
                }
                previousNode = node;
            }
            previousNode.next = newNode;
            size++;
            if (size > threshold) {
                resize();
            }
        }
        return null;
    }

    private V removeElementByKey(K key, Node<K, V> firstNode) {
        Node<K, V> previous = firstNode;
        for (; nonNull(firstNode); firstNode = firstNode.next) {
            if (firstNode.equalsKey(key)) {
                V oldValue = firstNode.value;
                previous.next = firstNode.next;
                firstNode.next = null;
                size--;
                return oldValue;
            }
            previous = firstNode;
        }
        return null;
    }

    private int tableSizeFor(int cap) {
        int n = -1 >>> Integer.numberOfLeadingZeros(cap - 1);
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    private Node<K, V> findElementByKey(K key, Node<K, V> firstNode) {
        for (Node<K, V> node = firstNode; nonNull(node); node = node.next) {
            if (key == node.key || (nonNull(key) && node.key.equals(key))) {
                return node;
            }
        }
        return null;
    }

    /**
     * Метод увеличивает размер внутренней хэш-таблицы. Используется, если таблица еще не создана,
     * либо, если таблица заполнена до максимально допустимого значения. При увелечении размера таблицы
     * происходит перераспределение элементов по новым индексам в таблице.
     */
    private void resize() {
        int oldCapacity = isNull(storage) ? 0 : storage.length;

        Node<K, V>[] oldTable = storage;
        storage = createNewStorage(oldCapacity);

        if (nonNull(oldTable)) {
            for (int i = 0; i < oldCapacity; i++) {
                Node<K, V> element = oldTable[i];

                if (nonNull(element)) {
                    oldTable[i] = null;
                    reallocateElements(element, i, oldCapacity);
                }
            }
        }
    }

    /**
     * Метод перераспределяет элемент но новое место.
     * Если элемент не один в цепочке, то вызывается отдельный метод, для распределения списка.
     *
     * @param element           элемент из старой таблицы
     * @param indexInOldStorage индекс элемента в старой хэш-таблице
     * @param oldCapacity       размер старой таблицы
     */
    private void reallocateElements(Node<K, V> element, int indexInOldStorage, int oldCapacity) {
        if (isNull(element.next)) {
            int index = getIndexInStorage(element.hash);
            storage[index] = element;
        } else {
            reallocateChainOfElements(element, indexInOldStorage, oldCapacity);
        }
    }

    /**
     * Метод перераспределяет элементы из односвязного списка.
     *
     * @param element           первый элемент в списке
     * @param indexInOldStorage индекс элемента в старой хэш-таблице
     * @param oldCapacity       вместимость старой таблицы
     */
    private void reallocateChainOfElements(Node<K, V> element, int indexInOldStorage, int oldCapacity) {
        Node<K, V> lowHead = null, lowTail = null;
        Node<K, V> highHead = null, highTail = null;

        while (nonNull(element)) {
            if ((element.hash & oldCapacity) == 0) {
                if (isNull(lowTail)) {
                    lowHead = element;
                } else {
                    lowTail.next = element;
                }
                lowTail = element;
            } else {
                if (isNull(highTail)) {
                    highHead = element;
                } else {
                    highTail.next = element;
                }
                highTail = element;
            }
            element = element.next;
        }

        if (nonNull(lowTail)) {
            lowTail.next = null;
            storage[indexInOldStorage] = lowHead;
        }

        if (nonNull(highTail)) {
            highTail.next = null;
            storage[indexInOldStorage + oldCapacity] = highHead;
        }
    }

    /**
     * Метод создает новую хэш-таблицу. Если размер таблицы уже равен максимально допустимому,
     * то размер не изменяется. Иначе размер новой таблицы будет вдвое больше прежней.
     *
     * @param oldCapacity прежний размер таблицы
     * @return новую таблицу
     */
    private Node<K, V>[] createNewStorage(int oldCapacity) {
        int oldThreshold = threshold;
        int newCapacity = 0;
        int newThreshold = 0;

        if (oldCapacity > 0) {
            if (oldCapacity >= MAXIMUM_CAPACITY) {
                threshold = Integer.MAX_VALUE;
                return storage;
            } else if ((newCapacity = oldCapacity << 1) < MAXIMUM_CAPACITY &&
                    oldCapacity >= DEFAULT_STORAGE_SIZE) {
                newThreshold = oldThreshold << 1;
            }
        } else if (oldThreshold > 0) {
            newCapacity = oldThreshold;
        } else {
            newCapacity = DEFAULT_STORAGE_SIZE;
            newThreshold = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_STORAGE_SIZE);
        }

        if (newThreshold == 0) {
            float tempThreshold = (float) newCapacity * loadFactor;
            if ((newCapacity < MAXIMUM_CAPACITY) && (tempThreshold < (float) MAXIMUM_CAPACITY)) {
                newThreshold = (int) tempThreshold;
            } else {
                newThreshold = Integer.MAX_VALUE;
            }
        }

        threshold = newThreshold;
        Node<K, V>[] newTable = new Node[newCapacity];
        return newTable;
    }

    private int getHash(K key) {
        if (isNull(key)) {
            return 0;
        } else {
            int hash = key.hashCode();
            return hash ^ (hash >>> 16);
        }
    }

    private int getIndexInStorage(int hash) {
        int length = isNull(storage) ? 0 : storage.length - 1;
        return hash & length;
    }

    private class Node<K, V> {
        int hash;
        final K key;
        V value;
        Node<K, V> next;

        Node(int hash, K key, V value) {
            this.hash = hash;
            this.key = key;
            this.value = value;
        }

        boolean equalsKey(K otherKey) {
            return (key == otherKey || (otherKey != null && key.equals(otherKey)));
        }

        @Override
        public String toString() {
            return key + " = " + value + "; ";
        }
    }
}
