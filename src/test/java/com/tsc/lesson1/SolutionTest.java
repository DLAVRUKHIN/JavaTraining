package com.tsc.lesson1;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SolutionTest {

    @Test
    public void getPowerTwo() {
        assertThat(Solution.getPowerTwo(30)).isEqualTo(1_073_741_824);
    }

    @Test
    public void getSumPowerTwo() {
        assertThat(Solution.getSumPowerTwo(30, 30)).isEqualTo(2_147_483_648L);
    }

    @Test
    public void getSumPowerTwo_differentPower(){
        assertThat(Solution.getSumPowerTwo(10, 3)).isEqualTo(1_032);
    }

    @Test
    public void resetLowerBit() {
        assertThat(Solution.resetLowerBit(0b110_1100_1111, 8)).isEqualTo(0b110_0000_0000);
    }

    @Test
    public void setBitToOne() {
        assertThat(Solution.setBitToOne(0b1_0000_1111_1001, 10)).isEqualTo(0b1_0100_1111_1001);
    }

    @Test
    public void setBitToZero(){
        assertThat(Solution.setBitToZero(0b100_0100_1110_1110,10)).isEqualTo(0b100_0000_1110_1110);
    }

    @Test
    public void invertBit(){
        assertThat(Solution.invertBit(0b1101_1101, 1)).isEqualTo(0b1101_1111);
    }

    @Test
    public void getLowerBits(){
        assertThat(Solution.getLowerBits(0b1111_1111,4)).isEqualTo(0b1111);
    }

    @Test
    public void getBit(){
        assertThat(Solution.getBit(0b1_0110_0011_1000,8)).isEqualTo(0);
    }

    @Test
    public void binaryRepresentationOfNumber(){
        assertThat(Solution.binaryRepresentationOfNumber((byte) -30)).isEqualTo("11100010");
    }

    @Test
    void swapTwoVariablesAdd() {
        int[] except = {1,2};
        int[] actual = {2,1};
        Solution.swapTwoVariablesAdd(actual);
        assertThat(actual).contains(except);
    }

    @Test
    void swapTwoVariablesMultiplication() {
        int[] except = {1,2};
        int[] actual = {2,1};
        Solution.swapTwoVariablesMultiplication(actual);
        assertThat(actual).contains(except);
    }

    @Test
    void swapTwoVariablesXOR() {
        int[] except = {1,2};
        int[] actual = {2,1};
        Solution.swapTwoVariablesXOR(actual);
        assertThat(actual).contains(except);
    }

    @Test
    void swapTwoVarsMix() {
        int[] except = {1,2};
        int[] actual = {2,1};
        Solution.swapTwoVarsMix(actual);
        assertThat(actual).contains(except);
    }
}
