package com.tsc.lesson6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AssociativeArrayTest {
    private AssociativeArray<String, String> associativeArray;

    @BeforeEach
    void setUp() {
        associativeArray = new AssociativeArray<>();
    }

    @Test
    void constructor_MustThrowException_IfIllegalCapacity() {
        int illegalInitialCapacity = -1;

        Throwable result = catchThrowable(() -> new AssociativeArray<String, String>(illegalInitialCapacity));
        assertThat(result)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Illegal initial capacity: " + illegalInitialCapacity);
    }

    @Test
    void constructor_MustThrowException_IfIllegalLoadFactor() {
        float illegalLoadFactor = -1;

        Throwable result = catchThrowable(() -> new AssociativeArray<String, String>(16, illegalLoadFactor));
        assertThat(result)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Illegal load factor: " + illegalLoadFactor);
    }

    @Test
    void add_MustAddElementInCollection() {
        String key = "key";
        String value = "value";
        associativeArray.add(key, value);
        assertThat(associativeArray.get(key)).isEqualTo(value);
    }

    @Test
    void add_MustReturnOldValue_IfKeyAlreadyExist() {
        String key = "key";
        String value1 = "value1";
        String value2 = "value2";

        associativeArray.add(key, value1);
        assertThat(associativeArray.add(key, value2)).isEqualTo(value1);
    }

    @Test
    void add_MustSetNewValue_IfKeyAlreadyExist() {
        String key = "key";
        String value1 = "value1";
        String value2 = "value2";

        associativeArray.add(key, value1);
        assertThat(associativeArray.get(key)).isEqualTo(value1);
    }

    @Test
    void add_MustAddElementInCollection_IfKeyIsNull() {
        String value = "value";
        associativeArray.add(null, value);
        assertThat(associativeArray.get(null)).isEqualTo(value);
    }

    @Test
    void addElementsWithSpecificKey_MustAddElement() {
        String specificKey1 = "polygenelubricants";
        String value1 = "value1";
        String specificKey2 = "random";
        String value2 = "value2";
        associativeArray.add(specificKey1, value1);
        associativeArray.add(specificKey2, value2);
        assertThat(associativeArray.get(specificKey1)).isEqualTo(value1);
        assertThat(associativeArray.get(specificKey2)).isEqualTo(value2);
    }

    @Test
    void addElementsMoreThanDefaultInitialCapacity_MustSaveOldElements() {
        for (int i = 0; i < 32; i++) {
            associativeArray.add("" + i, "value_" + i);
        }

        for (int i = 0; i < 32; i++) {
            assertThat(associativeArray.get("" + i))
                    .isEqualTo("value_" + i);
        }
    }

    @Test
    void get_MustReturnElementValue_IfElementExist() {
        String key = "key";
        String value = "value";
        associativeArray.add(key, value);
        assertThat(associativeArray.get(key)).isEqualTo(value);
    }

    @Test
    void get_MustReturnNull_IfElementNotExist() {
        String key = "key";
        assertThat(associativeArray.get(key)).isNull();
    }

    @Test
    void get_MustReturnElementValue_IfKeyIsNull() {
        String value = "value";

        associativeArray.add(null, value);
        assertThat(associativeArray.get(null)).isEqualTo(value);
    }

    @Test
    void remove_MustRemovedElementByKey_IfElementExist() {
        String key = "key";
        String value = "value";
        associativeArray.add(key, value);
        associativeArray.remove(key);
        assertThat(associativeArray.get(key)).isNull();
    }

    @Test
    void remove_MustReturnValueRemovedElement_IfElementExist() {
        String key = "key";
        String value = "value";
        associativeArray.add(key, value);
        assertThat(associativeArray.remove(key)).isEqualTo(value);
    }

    @Test
    void remove_MustReturnNull_IfElementNotExist() {
        String key = "key";
        assertThat(associativeArray.remove(key)).isNull();
    }

    @Test
    void remove_MustRemoveElement_IfKeyIsNull() {
        String value = "value";
        associativeArray.add(null, value);
        associativeArray.remove(null);
        assertThat(associativeArray.get(null)).isNull();
    }

    @Test
    void remove_MustReturnValueRemovedElement_IfKeyIsNull() {
        String value = "value";
        associativeArray.add(null, value);
        assertThat(associativeArray.remove(null))
                .isEqualTo(value);
    }

    @Test
    void toString_StringMustContainElements() {
        String[] keys = {"key1", "key2", "key3"};
        String[] values = {"value1", "value2", "value3"};
        String[] expected = new String[3];

        for (int i = 0; i < keys.length; i++) {
            expected[i] = keys[i] + " = " + values[i];
            associativeArray.add(keys[i], values[i]);
        }

        for (String s : expected) {
            assertTrue(associativeArray.toString().contains(s));
        }
    }
}

