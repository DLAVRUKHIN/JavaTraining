package com.tsc.lesson6;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.fail;

public class AssociativeArrayPerformanceTest {

    private final static String logFileName = "src/test/java/com/tsc/lesson6/result/AssociativeArrayPerformanceTest.txt";
    private static final float EXPECTED_TIME_RATIO = 0.2f;
    private final int count_element = 10_000_000;
    private final StopWatch timer = new StopWatch();
    private HashMap<String, String> map;
    private AssociativeArray<String, String> associativeArray;

    @BeforeAll
    static void clearFile() {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName))) {
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @BeforeEach
    void setUp() {
        map = new HashMap<>();
        associativeArray = new AssociativeArray<>();
    }

    @Test
    public void compareAddOperation() {
        String caption = "===============Add=================\r\n";

        timer.start();
        for (int i = 0; i < count_element; i++) {
            map.put("key " + i, "val");
        }
        long jdkHashMapTime = timer.getElapsedTime();

        timer.start();
        for (int i = 0; i < count_element; i++) {
            associativeArray.add("key" + i, "val");
        }
        long associativeArrayTime = timer.getElapsedTime();

        String jdkHashMapStringResult = createOperationResultString("HashMap", "add", jdkHashMapTime);
        String associativeArrayStringResult = createOperationResultString("AssociativeArray", "add", associativeArrayTime);
        writeInFile(caption, jdkHashMapStringResult, associativeArrayStringResult);
    }


    @Test
    public void compareRemoveOperation() {
        String caption = "===============Remove=================\r\n";

        for (int i = 0; i < count_element; i++) {
            map.put("key " + i, "val");
        }
        timer.start();
        for (int i = 0; i < count_element; i++) {
            map.remove("key " + i);
        }
        long jdkHashMapTime = timer.getElapsedTime();

        for (int i = 0; i < count_element; i++) {
            associativeArray.add("key" + i, "val");
        }
        timer.start();
        for (int i = 0; i < count_element; i++) {
            associativeArray.remove("key" + i);
        }
        long associativeArrayTime = timer.getElapsedTime();

        String jdkHashMapStringResult = createOperationResultString("HashMap", "remove", jdkHashMapTime);
        String associativeArrayStringResult = createOperationResultString("AssociativeArray", "remove", associativeArrayTime);
        writeInFile(caption, jdkHashMapStringResult, associativeArrayStringResult);
    }

    private String createOperationResultString(final String className,
                                               final String operationType,
                                               final long time) {

        StringBuilder resultString = new StringBuilder();
        resultString.append(className)
                .append(".")
                .append(operationType)
                .append(" : ")
                .append(time)
                .append(" ms\r\n");

        return resultString.toString();
    }

    private void writeInFile(String caption, String time1, String time2) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            writer.write(caption);
            writer.write(time1);
            writer.write(time2);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void checkRatio(long timeDLL, long timeLL) {
        long compare = timeDLL - timeLL;
        float actualRatio = (float) compare / timeLL;

        if (compare > 0 && actualRatio > EXPECTED_TIME_RATIO) {
            fail("AssociativeArray exceeded time HashMap for " + actualRatio * 100 + "%");
        }
    }

}
