package com.tsc.lesson10;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static com.tsc.lesson10.ArraySorting.quickSort;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class ArraySortingTest {

    int countElement = 30_000_000;

    @Test
    public void quickSort_shouldNotReturnAnError_WhenArrayIsNull(){
        int[] array = null;
        Throwable result = catchThrowable(()->quickSort(array));
        assertThat(result).isNull();

    }

    @Test
    public void quickSort_MustSortingArray() {
        int[] array = new int[countElement];
        Random random = new Random();
        for (int i = 0; i < countElement; i++) {
            array[i] = random.nextInt();
        }
        quickSort(array);
        assertThat(array).isSorted();
    }
}
