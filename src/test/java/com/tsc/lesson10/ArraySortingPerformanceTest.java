package com.tsc.lesson10;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ArraySortingPerformanceTest {

    static int countElement = 30_000_000;
    static String logFileName = "src/test/java/com/tsc/lesson10/result/ArraySortingPerformanceTest.txt";
    StopWatch timer = new StopWatch();
    static int[] array;
    static List<Integer> list;

    @BeforeAll
    public static void createArray() {
        clearFile();
        Random random = new Random();
        array = new int[countElement];
        list = new ArrayList<>();
        int element;
        for (int i = 0; i < countElement; i++) {
            element = random.nextInt();
            array[i] = element;
            list.add(element);
        }
    }

    private static void clearFile() {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName))) {
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void sort_ArrayWithRandomValue() {
        int[] array1 = array.clone();
        int[] array2 = array.clone();

        timer.start();
        Arrays.sort(array1);
        long jdkArraysSortResult = timer.getElapsedTime();
        writeToFile("Arrays", jdkArraysSortResult);

        timer.start();
        ArraySorting.quickSort(array2);
        long arraySortingResult = timer.getElapsedTime();
        writeToFile("ArraySorting", arraySortingResult);

        timer.start();
        Collections.sort(list);
        long collectionsSortResult = timer.getElapsedTime();
        writeToFile("Collection", collectionsSortResult);
    }

    private void writeToFile(String className, long time) {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName, true))) {
            write.write(className + ".sort() " + time + " ms\r\n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }


}
