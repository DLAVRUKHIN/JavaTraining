package com.tsc.lesson2;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

class HelloWorldAlbanianTest {

    @Test
    public void testMain(){
        PrintStream out = System.out;
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        System.setOut(new PrintStream(result));
        HelloWorldAlbanian.main(null);
        System.setOut(out);
        assertThat(result.toString()).isEqualTo("Përshëndetje botë!\r\n");
    }

}
