package com.tsc.util;

public class StopWatch {

    private long startTime;

    public long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public long getElapsedTime() {
        long endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        return time;
    }
}

