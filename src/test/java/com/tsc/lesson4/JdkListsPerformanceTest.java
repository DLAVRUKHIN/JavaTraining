package com.tsc.lesson4;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JdkListsPerformanceTest {
    private final static String logFileName = "src/test/java/com/tsc/lesson4/result/JDKListPerformanceTest.txt";

    private final int count_min = 20_000;
    private final int count_max = 200_000;
    private final StopWatch timer = new StopWatch();
    List<String> arrayList;
    List<String> linkedList;

    @BeforeAll
    static void clearFile() {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName))) {
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @BeforeEach
    void setUp() {
        arrayList = new ArrayList<>();
        linkedList = new LinkedList<>();
    }

    @Test
    void compareAddToBegin() {
        String caption = "=========Add to begin===========\r\n";
        long arrayListTime = calculateTimeToAddItemsInBeginList(arrayList);
        long linkedListTime = calculateTimeToAddItemsInBeginList(linkedList);
        String arrayListStringResult = createOperationResultString("ArrayList", "add(0)", arrayListTime);
        String linkedListStringResult = createOperationResultString("LinkedList", "add(0)", linkedListTime);
        writeInFile(caption, linkedListStringResult, arrayListStringResult);
    }

    @Test
    void compareAddToMiddle() {
        String caption = "=========Add to middle===========\r\n";
        long arrayListTime = calculateTimeToAddItemsInMiddleList(arrayList);
        long linkedListTime = calculateTimeToAddItemsInMiddleList(linkedList);
        String arrayListStringResult = createOperationResultString("ArrayList", "add(i)", arrayListTime);
        String linkedListStringResult = createOperationResultString("LinkedList", "add(i)", linkedListTime);
        writeInFile(caption, linkedListStringResult, arrayListStringResult);
    }

    @Test
    void compareAddToEnd() {
        String caption = "=========Add to end===========\r\n";
        long arrayListTime = calculateTimeToAddItemsInEndList(arrayList);
        long linkedListTime = calculateTimeToAddItemsInEndList(linkedList);
        String arrayListStringResult = createOperationResultString("ArrayList", "add()", arrayListTime);
        String linkedListStringResult = createOperationResultString("LinkedList", "add()", linkedListTime);
        writeInFile(caption, linkedListStringResult, arrayListStringResult);
    }

    @Test
    void compareRemoveFromBegin() {
        String caption = "=========Remove from begin===========\r\n";
        long arrayListTime = calculateTimeToRemoveItemsFromBeginList(arrayList);
        long linkedListTime = calculateTimeToRemoveItemsFromBeginList(linkedList);
        String arrayListStringResult = createOperationResultString("ArrayList", "remove(0)", arrayListTime);
        String linkedListStringResult = createOperationResultString("LinkedList", "remove(0)", linkedListTime);
        writeInFile(caption, linkedListStringResult, arrayListStringResult);
    }

    @Test
    void compareRemoveFromMiddle() {
        String caption = "=========Remove from middle===========\r\n";
        long arrayListTime = calculateTimeToRemoveItemsFromMiddleList(arrayList);
        long linkedListTime = calculateTimeToRemoveItemsFromMiddleList(linkedList);
        String linkedListStringResult = createOperationResultString("LinkedList", "remove(i)", linkedListTime);
        String arrayListStringResult = createOperationResultString("ArrayList", "remove(i)", arrayListTime);
        writeInFile(caption, linkedListStringResult, arrayListStringResult);
    }

    @Test
    void compareRemoveFromEnd() {
        String caption = "=========Remove from end===========\r\n";
        long arrayListTime = calculateTimeToRemoveItemsFromEndList(arrayList);
        long linkedListTime = calculateTimeToRemoveItemsFromEndList(linkedList);
        String arrayListStringResult = createOperationResultString("ArrayList", "remove(i)", arrayListTime);
        String linkedListStringResult = createOperationResultString("LinkedList", "remove(i)", linkedListTime);
        writeInFile(caption, linkedListStringResult, arrayListStringResult);
    }

    private long calculateTimeToAddItemsInBeginList(List list) {
        timer.start();
        for (int i = 0; i < count_max; i++) {
            list.add(0, "string");
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToAddItemsInMiddleList(List list) {
        timer.start();
        for (int i = 0; i < count_min; i++) {
            list.add(i / 2, "string");
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToAddItemsInEndList(List list) {
        timer.start();
        for (int i = 0; i < count_max; i++) {
            list.add("string");
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToRemoveItemsFromBeginList(List list) {
        for (int i = 0; i < count_max; i++) {
            list.add("string");
        }
        timer.start();
        for (int i = 0; i < count_max; i++) {
            list.remove(0);
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToRemoveItemsFromMiddleList(List list) {
        for (int i = 0; i < count_min; i++) {
            list.add("string");
        }
        timer.start();
        for (int i = list.size() - 1; i > 0; i--) {
            list.remove(i / 2);
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToRemoveItemsFromEndList(List list) {
        for (int i = 0; i < count_max; i++) {
            list.add("string");
        }
        timer.start();
        for (int i = list.size() - 1; i >= 0; i--) {
            list.remove(i);
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private String createOperationResultString(final String className,
                                               final String methodName,
                                               final long time) {

        StringBuilder resultString = new StringBuilder();
        resultString.append(className)
                .append(".")
                .append(methodName)
                .append(" : ")
                .append(time)
                .append(" ms\r\n");

        return resultString.toString();
    }

    private void writeInFile(String caption,
                             String resultTime1,
                             String resultTime2) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            writer.write(caption);
            writer.write(resultTime1);
            writer.write(resultTime2);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
