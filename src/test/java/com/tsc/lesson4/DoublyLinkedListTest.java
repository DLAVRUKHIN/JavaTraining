package com.tsc.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class DoublyLinkedListTest {

    DoublyLinkedList<String> linkedList;

    @BeforeEach
    void createDoublyLinkedList() {
        linkedList = new DoublyLinkedList<>();
    }

    @Test
    void add_MustArrayIndexOutOfBoundsException_WhenIndexOutOfBounds() {
        Throwable result = catchThrowable(() -> linkedList.add(124, "String 1"));
        assertThat(result).isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }

    @Test
    void addInEnd_MustReturnElement() {
        String except = "String 1";
        linkedList.add("s1");
        linkedList.add(except);
        assertThat(linkedList.get(1)).isEqualTo(except);
    }

    @Test
    void addInBegin_MustReturnElement() {
        linkedList.add("S1");
        linkedList.add("S1");
        linkedList.add("S1");
        String except = "String 1";
        linkedList.add(0, except);
        assertThat(linkedList.get(0)).isEqualTo(except);
    }

    @Test
    void addInMiddle_MustReturnElement() {
        String except = "String 1";
        linkedList.add("S1");
        linkedList.add("S1");
        linkedList.add("S1");
        linkedList.add("S1");
        linkedList.add(3, except);
        assertThat(linkedList.get(3)).isEqualTo(except);
    }

    @Test
    void size_ReturnCountElement() {
        linkedList.add("String 1");
        linkedList.add("String 1");
        linkedList.add("String 1");
        linkedList.add("String 1");
        linkedList.add("String 1");
        assertThat(linkedList.size()).isEqualTo(5);
    }

    @Test
    void contains_MustReturnTrue_IfElementContainsInList() {
        String except = "String 1";
        linkedList.add(except);
        linkedList.add("String 2");
        assertThat(linkedList.contains(except)).isTrue();
    }

    @Test
    void contains_MustReturnFalse_IfElementNotContainsInList() {
        linkedList.add("String 1");
        linkedList.add("String 2");
        linkedList.add("String 3");
        assertThat(linkedList.contains("String 4")).isFalse();
    }

    @Test
    void remove_MustRemovedElementByIndex() {
        String except = "String 2";
        linkedList.add("String 1");
        linkedList.add(except);
        linkedList.add("String 3");
        linkedList.remove(1);
        assertThat(linkedList.toArray()).doesNotContain(except);
    }

    @Test
    void remove_MustRemovedElementByValue() {
        String except = "String 2";
        linkedList.add("String 1");
        linkedList.add(except);
        linkedList.add("String 3");
        linkedList.remove(except);
        assertThat(linkedList.toArray()).doesNotContain(except);
    }

    @Test
    void remove_MustReturnTrue_WhenElementNotContains(){
        String except = "String 2";
        linkedList.add("String 1");
        linkedList.add(except);
        linkedList.add("String 3");
        linkedList.add("String 4");
        boolean result = linkedList.remove(except);
        assertThat(result).isTrue();
    }

    @Test
    void remove_MustReturnFalse_WhenElementNotContains(){
        linkedList.add("String 1");
        linkedList.add("String 2");
        linkedList.add("String 3");
        linkedList.add("String 4");
        boolean result = linkedList.remove("String 6");
        assertThat(result).isFalse();
    }

    @Test
    void indexOf_MustReturnIndexElementInList_WhenListContainsElement(){
        String except = "String 2";
        linkedList.add("String 1");
        linkedList.add(except);
        linkedList.add("String 3");
        linkedList.add("String 4");
        int idx = linkedList.indexOf(except);
        assertThat(idx).isEqualTo(1);
    }

    @Test
    void indexOf_MustReturnMinusOne_WhenListNotContainsElement(){
        linkedList.add("String 1");
        linkedList.add("String 3");
        linkedList.add("String 4");
        int idx = linkedList.indexOf("String 11");
        assertThat(idx).isEqualTo(-1);
    }

    @Test
    void set_MustChangeValueElement_WhenListContainsElement(){
        String except = "new Value";
        linkedList.add("String 1");
        linkedList.add("String 3");
        linkedList.add("String 4");
        linkedList.set(1, except);
        assertThat(linkedList.get(1)).isEqualTo(except);
    }

    @Test
    void set_MustArrayIndexOutOfBoundsException_WhenIndexOutOfBounds() {
        Throwable result = catchThrowable(() -> linkedList.set(124, "String 1"));
        assertThat(result).isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }

    @Test
    void toArray_MustReturnArrayElement(){
        String[] exceptArray = {"String 1", "String 2", "String 3", "String 4"};
        for (String s: exceptArray){
            linkedList.add(s);
        }
        assertThat(linkedList.toArray()).isEqualTo(exceptArray);
    }
//TODO добавить проверки когда null элементы
}
