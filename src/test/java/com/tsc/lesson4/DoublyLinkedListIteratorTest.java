package com.tsc.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class DoublyLinkedListIteratorTest {

    DoublyLinkedList<String> linkedList = new DoublyLinkedList<>();
    ListIterator iterator;

    @BeforeEach
    void createIterator() {
        linkedList.add("String 1");
        linkedList.add("String 2");
        iterator = linkedList.listIterator();
    }

    @Test
    void next_mustReturnObject_whenArrayHasElement() {
        Object result = iterator.next();
        assertThat(result).isEqualTo("String 1");
    }

    @Test
    void next_mustThrowNoSuchElementException_whenArrayNoNextElement() {
        iterator.next();
        iterator.next();
        Throwable result = catchThrowable(() -> iterator.next());
        assertThat(result).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void next_mustConcurrentModificationException_whenArrayModified() {
        linkedList.add("String 1");
        Throwable result = catchThrowable(() -> iterator.next());
        assertThat(result).isInstanceOf(ConcurrentModificationException.class);
    }

    @Test
    void previous_mustConcurrentModificationException_whenArrayModified() {
        linkedList.add("String 1");
        Throwable result = catchThrowable(() -> iterator.previous());
        assertThat(result).isInstanceOf(ConcurrentModificationException.class);
    }

    @Test
    void previous_mustThrowNoSuchElementException_whenArrayNoPreviousElement() {
        Throwable result = catchThrowable(() -> iterator.previous());
        assertThat(result).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void previous_mustReturnObject_whenArrayHasPreviousElement() {
        iterator.next();
        Object result = iterator.previous();
        assertThat(result).isEqualTo("String 1");
    }

    @Test
    void hasNext_mustReturnTrue_whenArrayHasNextElement() {
        boolean result = iterator.hasNext();
        assertThat(result).isTrue();
    }

    @Test
    void hasNext_mustReturnFalse_whenArrayNoNextElement() {
        iterator.next();
        iterator.next();
        boolean result = iterator.hasNext();
        assertThat(result).isFalse();
    }

    @Test
    void hasPrevious_mustReturnTrue_whenArrayHasPreviousElement() {
        iterator.next();
        boolean result = iterator.hasPrevious();
        assertThat(result).isTrue();
    }

    @Test
    void hasPrevious_mustReturnFalse_whenArrayNoPreviousElement() {
        boolean result = iterator.hasPrevious();
        assertThat(result).isFalse();
    }

    @Test
    void nextIndex_mustReturnIndex() {
        int result = iterator.nextIndex();
        assertThat(result).isEqualTo(0);
    }

    @Test
    void previousIndex_mustReturnIndex_whenArrayHasPreviousElement() {
        iterator.next();
        int result = iterator.previousIndex();
        assertThat(result).isEqualTo(0);
    }

    @Test
    void previousIndex_mustReturnIndex_whenArrayNoPreviousElement() {
        int result = iterator.previousIndex();
        assertThat(result).isEqualTo(-1);
    }

    @Test
    void remove_modifiedArraySize_whenElementSelected() {
        iterator.next();
        iterator.remove();
        int result = linkedList.size();
        assertThat(result).isEqualTo(1);
    }

    @Test
    void remove_mustConcurrentModificationException_whenArrayModified() {
        iterator.next();
        linkedList.remove(0);
        Throwable result = catchThrowable(() -> iterator.remove());
        assertThat(result).isInstanceOf(ConcurrentModificationException.class);
    }

    @Test
    void remove_mustIllegalStateException_whenElementNotSelected() {
        Throwable result = catchThrowable(() -> iterator.remove());
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void remove_mustIllegalStateException_afterSecondRemove() {
        iterator.next();
        iterator.remove();
        Throwable result = catchThrowable(() -> iterator.remove());
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void set_modifiedArrayElement_whenElementSelected() {
        String newValue = "change string";
        iterator.next();
        iterator.set(newValue);
        assertThat(linkedList.get(0)).isEqualTo(newValue);
    }

    @Test
    void set_mustIllegalStateException_whenElementNotSelected() {
        Throwable result = catchThrowable(() -> iterator.set("new string"));
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void set_mustIllegalStateException_afterAddElementIterators() {
        iterator.add("new string");
        Throwable result = catchThrowable(() -> iterator.set("new string"));
        assertThat(result).isInstanceOf(IllegalStateException.class);
    }

    @Test
    void set_mustConcurrentModificationException_whenArrayModified() {
        iterator.next();
        linkedList.remove(0);
        Throwable result = catchThrowable(() -> iterator.set("new string"));
        assertThat(result).isInstanceOf(ConcurrentModificationException.class);
    }

    @Test
    void add_modifiedArraySize_whenElementSelected() {
        String newElement = "new String";
        iterator.add(newElement);
        assertThat(linkedList.get(0)).isEqualTo(newElement);
        assertThat(linkedList.size()).isEqualTo(3);
    }

    @Test
    void add_mustConcurrentModificationException_whenArrayModified() {
        linkedList.remove(0);
        Throwable result = catchThrowable(() -> iterator.add("new string"));
        assertThat(result).isInstanceOf(ConcurrentModificationException.class);
    }

}
