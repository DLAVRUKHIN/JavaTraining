package com.tsc.lesson4;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class DoublyLinkedListPerformanceTest {

    private final static String logFileName = "src/test/java/com/tsc/lesson4/result/DoublyLinkedListPerformanceTest.txt";

    private final int count_min = 20_000;
    private final int count_max = 200_000;
    private final StopWatch timer = new StopWatch();
    List<String> jdkLinkedList;
    List<String> doublyLinkedList;


    @BeforeAll
    static void clearFile() {
        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName))) {
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @BeforeEach
    void setUp() {
        jdkLinkedList = new LinkedList<>();
        doublyLinkedList = new DoublyLinkedList<>();
    }

    @Test
    void compareAddToBegin() {
        String caption = "=========Add to begin===========\r\n";
        long jdkLinkedListTime = calculateTimeToAddItemsInBeginList(jdkLinkedList);
        long doublyLinkedListTime = calculateTimeToAddItemsInBeginList(doublyLinkedList);
        String jdkStringResult = createOperationResultString("LinkedList", "add(0,e)", jdkLinkedListTime);
        String doublyLinkedListStringResult = createOperationResultString("DoublyLinkedList", "add(0,e)", doublyLinkedListTime);
        writeInFile(caption, jdkStringResult, doublyLinkedListStringResult);
    }

    @Test
    void compareAddToMiddle() {
        String caption = "=========Add to middle===========\r\n";
        long jdkLinkedListTime = calculateTimeToAddItemsInMiddleList(jdkLinkedList);
        long doublyLinkedListTime = calculateTimeToAddItemsInMiddleList(doublyLinkedList);
        String jdkStringResult = createOperationResultString("LinkedList", "add(i,e)", jdkLinkedListTime);
        String doublyLinkedListStringResult = createOperationResultString("DoublyLinkedList", "add(i,e)", doublyLinkedListTime);
        writeInFile(caption, jdkStringResult, doublyLinkedListStringResult);
    }

    @Test
    void compareAddToEnd() {
        String caption = "=========Add to end===========\r\n";
        long jdkLinkedListTime = calculateTimeToAddItemsInEndList(jdkLinkedList);
        long doublyLinkedListTime = calculateTimeToAddItemsInEndList(doublyLinkedList);
        String jdkStringResult = createOperationResultString("LinkedList", "add(e)", jdkLinkedListTime);
        String doublyLinkedListStringResult = createOperationResultString("DoublyLinkedList", "add(e)", doublyLinkedListTime);
        writeInFile(caption, jdkStringResult, doublyLinkedListStringResult);
    }

    @Test
    void compareRemoveFromBegin() {
        String caption = "=========Remove from begin===========\r\n";
        long jdkLinkedListTime = calculateTimeToRemoveItemsFromBeginList(jdkLinkedList);
        long doublyLinkedListTime = calculateTimeToRemoveItemsFromBeginList(doublyLinkedList);
        String jdkStringResult = createOperationResultString("LinkedList", "remove(0)", jdkLinkedListTime);
        String doublyLinkedListStringResult = createOperationResultString("DoublyLinkedList", "remove(0)", doublyLinkedListTime);
        writeInFile(caption, jdkStringResult, doublyLinkedListStringResult);
    }

    @Test
    void compareRemoveFromMiddle() {
        String caption = "=========Remove from middle===========\r\n";
        long doublyLinkedListTime = calculateTimeToRemoveItemsFromMiddleList(doublyLinkedList);
        long jdkLinkedListTime = calculateTimeToRemoveItemsFromMiddleList(jdkLinkedList);
        String jdkStringResult = createOperationResultString("LinkedList", "remove(i)", jdkLinkedListTime);
        String doublyLinkedListStringResult = createOperationResultString("DoublyLinkedList", "remove(i)", doublyLinkedListTime);
        writeInFile(caption, jdkStringResult, doublyLinkedListStringResult);
    }

    @Test
    void compareRemoveFromEnd() {
        String caption = "=========Remove from end===========\r\n";
        long jdkLinkedListTime = calculateTimeToRemoveItemsFromEndList(jdkLinkedList);
        long doublyLinkedListTime = calculateTimeToRemoveItemsFromEndList(doublyLinkedList);
        String jdkStringResult = createOperationResultString("LinkedList", "remove(i)", jdkLinkedListTime);
        String doublyLinkedListStringResult = createOperationResultString("DoublyLinkedList", "remove(i)", doublyLinkedListTime);
        writeInFile(caption, jdkStringResult, doublyLinkedListStringResult);
    }


    private long calculateTimeToAddItemsInBeginList(List list) {
        timer.start();
        for (int i = 0; i < count_max; i++) {
            list.add(0, "string");
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToAddItemsInMiddleList(List list) {
        timer.start();
        for (int i = 0; i < count_min; i++) {
            list.add(i / 2, "string");
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToAddItemsInEndList(List list) {
        timer.start();
        for (int i = 0; i < count_max; i++) {
            list.add("string");
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToRemoveItemsFromBeginList(List list) {
        for (int i = 0; i < count_max; i++) {
            list.add("string");
        }
        timer.start();
        for (int i = 0; i < count_max; i++) {
            list.remove(0);
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToRemoveItemsFromMiddleList(List list) {
        for (int i = 0; i < count_min; i++) {
            list.add("string");
        }
        timer.start();
        for (int i = list.size() - 1; i > 0; i--) {
            list.remove(i / 2);
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private long calculateTimeToRemoveItemsFromEndList(List list) {
        for (int i = 0; i < count_max; i++) {
            list.add("string");
        }
        timer.start();
        for (int i = list.size() - 1; i >= 0; i--) {
            list.remove(i);
        }
        long elapsedTime = timer.getElapsedTime();
        return elapsedTime;
    }

    private String createOperationResultString(final String className,
                                               final String operationType,
                                               final long time) {

        StringBuilder resultString = new StringBuilder();
        resultString.append(className)
                .append(".")
                .append(operationType)
                .append(" : ")
                .append(time)
                .append(" ms\r\n");

        return resultString.toString();
    }

    private void writeInFile(String caption, String time1, String time2) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            writer.write(caption);
            writer.write(time1);
            writer.write(time2);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

