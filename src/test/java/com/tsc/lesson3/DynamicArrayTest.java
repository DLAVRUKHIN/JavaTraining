package com.tsc.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class DynamicArrayTest {

    DynamicArray da;

    @BeforeEach
    void createDynamicArray() {
        da = new DynamicArray();
    }

    @Test
    void constructor_MustIllegalArgumentException_WhenArgumentIsNegative() {
        final String failMsg = "Illegal Capacity: -10";
        try {
            DynamicArray da = new DynamicArray(-10);
            fail(failMsg);
        } catch (IllegalArgumentException exception) {
            assertThat(exception.getMessage()).isEqualTo(failMsg);
        }
    }

    @Test
    void constructor_MustChangeSize_WhenParameterArray() {
        Object[] exceptArray = {"String 1", 123, 'a'};
        da = new DynamicArray(exceptArray);
        assertThat(da.size()).isEqualTo(3);
    }

    @Test
    void constructor_MustChangeInnerArray_whenParameterArray() {
        Object[] exceptArray = {"String 1", 123, 'a'};
        da = new DynamicArray(exceptArray);
        assertThat(da.toArray()).isEqualTo(exceptArray);
    }

    @Test
    void constructor_MustChangeSize_WhenParameterCapacity() {
        da = new DynamicArray(1);
        da.add("String 1");
        da.add("String 1");
        da.add("String 1");
        assertThat(da.size()).isEqualTo(3);
    }

    @Test
    void add_MustContainsElement() {
        da.add("String 1");
        da.add(null);
        da.add("String 2");
        assertThat(da.toArray()[1]).isNull();
        assertThat(da.toArray()).contains("String 1").contains("String 2");
    }

    @Test
    void add_MustContainsElement_whenAddObjectByIndex() {
        da.add("String 1");
        da.add("String 2");
        da.add(1, "String 3");
        da.add(0, "first string");
        da.add(4, "last string");
        assertThat(da.toArray()[0]).isEqualTo("first string");
    }

    @Test
    void add_MustArrayIndexOutOfBoundsException_IndexGreaterSize() {
        final String failMsg = "Array index out of range: 101";
        try {
            da.add(101, "String");
            fail(failMsg);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertThat(e.getMessage()).isEqualTo(failMsg);
        }
    }

    @Test
    void set_MustChangeElement_WhenIndexExistent() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        da.set(1, "String add");
        assertThat(da.toArray()[1]).isEqualTo("String add");
    }

    @Test
    void set_MustArrayIndexOutOfBoundsException_NonExistentIndex() {
        final String failMsg = "Array index out of range: 101";
        try {
            da.set(101, "new String");
            fail(failMsg);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertThat(e.getMessage()).isEqualTo(failMsg);
        }
    }

    @Test
    void get_MustReturnObjectByIndex_WhenIndexExisting() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.toArray()[2]).isEqualTo("String 3");
    }

    @Test
    void get_MustArrayIndexOutOfBoundsException_NonExistentIndex() {
        final String failMsg = "Array index out of range: 101";
        try {
            da.get(101);
            fail(failMsg);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertThat(e.getMessage()).isEqualTo(failMsg);
        }
    }

    @Test
    void remove_MustRemoveOnlyOneElement_WhenElementExisting() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 2");
        da.add("String 3");
        da.remove("String 2");
        assertThat(da.toArray()).containsOnlyOnce("String 2");
    }

    @Test
    void remove_MustRemoveElement_WhenElementIsNull(){
        da.add("String 1");
        da.add(null);
        da.add("String 2");
        boolean result = da.remove(null);
        assertThat(result).isTrue();
    }

    @Test
    void remove_MustReturnFalse_WhenElementNonExisting() {
        da.add("String 1");
        da.add("String 2");
        boolean result = da.remove("String 5");
        assertThat(result).isFalse();
    }

    @Test
    void remove_MustRemoveElementByIndex_WhenIndexExisting() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        da.remove(1);
        assertThat(da.toArray()[1]).isEqualTo("String 3");
    }

    @Test
    void remove_MustRemoveElementByIndex_WhenIndexNonExisting() {
        final String failMsg = "Array index out of range: 101";

        da.add("String 1");
        da.add("String 2");
        da.add("String 3");

        try {
            da.remove(101);
            fail(failMsg);
        } catch (ArrayIndexOutOfBoundsException e) {
            assertThat(e.getMessage()).isEqualTo(failMsg);
        }
    }

    @Test
    void size_MustChangeValue_AfterAddElement() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.size()).isEqualTo(3);
    }

    @Test
    void indexOf_MustReturnIndexOfElement_WhenElementExists() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.indexOf("String 2")).isEqualTo(1);
    }

    @Test
    void indexOf_MustReturnMinusOne_WhenElementNonExists() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.indexOf("String 211")).isEqualTo(-1);
    }

    @Test
    void contains_MustReturnTrue_WhenObjectContains() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.contains("String 3")).isTrue();
    }

    @Test
    void contains_MustReturnFalse_WhenObjectNotContains() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.contains("String 3123")).isFalse();
    }

    @Test
    void toArray_MustReturnInnerArray() {
        da.add("String 1");
        da.add("String 2");
        da.add("String 3");
        assertThat(da.toArray()).isEqualTo(new String[]{"String 1", "String 2", "String 3"});
    }
}
