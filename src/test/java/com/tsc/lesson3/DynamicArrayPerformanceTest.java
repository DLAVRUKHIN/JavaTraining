package com.tsc.lesson3;

import com.tsc.util.StopWatch;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DynamicArrayPerformanceTest {

    private final String logFileName = "DynamicArrayPerformanceTest.txt";

    private final int count = 500_000;
    private final StopWatch timer;

    {
        timer = new StopWatch();
    }

    @Test
    void compareAddOperationToArrayList() {
        addElementInBeginArrayList();
        addElementInBeginDynamicArray();
        addElementInMiddleArrayList();
        addElementInMiddleDynamicArray();
        addElementInEndArrayList();
        addElementInEndDynamicArray();

        removeElementFromTheBeginArrayList();
        removeElementFromTheBeginDynamicArray();
        removeElementInMiddleArrayList();
        removeElementInMiddleDynamicArray();
        removeElementFromTheEndArrayList();
        removeElementFromTheEndDynamicArray();
    }

    private void addElementInEndArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        timer.start();
        for (int i = 0; i < count; i++) {
            arrayList.add("string");
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("ArrayList", "add", "end", elapsedTime);
    }

    private void addElementInEndDynamicArray() {
        DynamicArray da = new DynamicArray();

        timer.start();
        for (int i = 0; i < count; i++) {
            da.add("string");
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("DynamicArray", "add", "end", elapsedTime);
    }

    private void addElementInMiddleArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        timer.start();
        for (int i = 0; i < count; i++) {
            arrayList.add(i / 2, "string");
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("ArrayList", "add", "end", elapsedTime);
    }

    private void addElementInMiddleDynamicArray() {
        DynamicArray da = new DynamicArray();

        timer.start();
        for (int i = 0; i < count; i++) {
            da.add(i / 2, "string");
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("DynamicArray", "add", "middle", elapsedTime);
    }

    private void addElementInBeginArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        timer.start();

        for (int i = 0; i < count; i++) {
            arrayList.add(0, "string");
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("ArrayList", "add", "begin", elapsedTime);
    }

    private void addElementInBeginDynamicArray() {
        DynamicArray da = new DynamicArray();

        timer.start();
        for (int i = 0; i < count; i++) {
            da.add(0, "string");
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("DynamicArray", "add", "begin", elapsedTime);
    }

    private void removeElementFromTheEndArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            arrayList.add("string");
        }

        timer.start();
        for (int i = arrayList.size() - 1; i >= 0; i--) {
            arrayList.remove(i);
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("ArrayList", "remove", "end", elapsedTime);
    }

    private void removeElementFromTheEndDynamicArray() {
        DynamicArray da = new DynamicArray();

        for (int i = 0; i < count; i++) {
            da.add("string");
        }

        timer.start();
        for (int i = da.size() - 1; i >= 0; i--) {
            da.remove(i);
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("DynamicArray", "remove", "end", elapsedTime);
    }

    private void removeElementInMiddleArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            arrayList.add(i / 2, "string");
        }

        timer.start();
        for (int i = arrayList.size()-1; i > 0; i--) {
            arrayList.remove(i / 2);
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("ArrayList", "remove", "middle", elapsedTime);
    }

    private void removeElementInMiddleDynamicArray() {
        DynamicArray da = new DynamicArray();

        for (int i = 0; i < count; i++) {
            da.add(i / 2, "string");
        }
        timer.start();
        for (int i = da.size()-1; i > 0; i--) {
            da.remove(i / 2);
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("DynamicArray", "remove", "middle", elapsedTime);
    }

    private void removeElementFromTheBeginArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            arrayList.add(0, "string");
        }
        timer.start();
        for (int i = 0; i < count; i++) {
            arrayList.remove(0);
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("ArrayList", "remove","begin", elapsedTime);
    }

    private void removeElementFromTheBeginDynamicArray() {
        DynamicArray da = new DynamicArray();

        for (int i = 0; i < count; i++) {
            da.add(0, "string");
        }

        timer.start();
        for (int i = 0; i < count; i++) {
            da.remove(0);
        }
        long elapsedTime = timer.getElapsedTime();

        writeInFile("DynamicArray", "remove","begin",elapsedTime);
    }

    private void writeInFile(final String className,
                             final String operationType,
                             final String position,
                             final long time) {

        StringBuilder stringToWrite = new StringBuilder();
        stringToWrite.append(className)
                .append(".")
                .append(operationType)
                .append(" in ")
                .append(position)
                .append(" : ")
                .append(time)
                .append(" ms\r\n");

        try (BufferedWriter write = new BufferedWriter(new FileWriter(logFileName, true))) {
            write.write(stringToWrite.toString());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
